import org.apache.commons.cli.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataSeparator {
    public static void main(String[] args) {
        Options options = getOptions();

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();

        try {
            CommandLine cmd = parser.parse(options, args);

            String inputFiles = cmd.getOptionValue("input");
            String outputDirectory = cmd.getOptionValue("output", ".");
            String filePrefix = cmd.getOptionValue("prefix", "");
            boolean appendMode = cmd.hasOption("append");
            boolean shortStatMode = cmd.hasOption("short-stat");
            boolean fullStatMode = cmd.hasOption("full-stat");

            processFiles(inputFiles, outputDirectory, filePrefix, appendMode, shortStatMode, fullStatMode);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("DataSeparator", options);
            System.exit(1);
        }
    }

    /**
     * Главныей метод обработки выходных файлов согласно указанным опциям.
     *
     * @param inputFiles строка, содержащая имена файлов, которые нужно обработать.
     *                   Имена разделены символом запятой без пробела.
     * @param outputDirectory директория, в которую будут сохранены файлы результатов.
     * @param filePrefix префикс для файлов результатов.
     * @param appendMode флаг, который отвечает за добавление данных в конец файлов результатов или перезаписывание их.
     *                   Если true, то данные будут дописываться в конец, иначе - файлы будут перезаписаны.
     * @param shortStatMode флаг, который отвечает за показ краткой статистики по обработанным файлам.
     * @param fullStatMode флаг, который отвечает за показ полной статистики по обработанным файлам.
     */
     static void processFiles(String inputFiles, String outputDirectory, String filePrefix, boolean appendMode, boolean shortStatMode, boolean fullStatMode) {
        String[] files = inputFiles.split(",");
        List<Long> integers = new ArrayList<>();
        List<Double> floats = new ArrayList<>();
        List<String> strings = new ArrayList<>();

        for (String file : files) {
            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    try {
                        long intValue = Long.parseLong(line);
                        integers.add(intValue);
                    } catch (NumberFormatException e1) {
                        try {
                            double floatValue = Double.parseDouble(line);
                            floats.add(floatValue);
                        } catch (NumberFormatException e2) {
                            strings.add(line);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!integers.isEmpty()) {
            writeToFile(outputDirectory, filePrefix + "integers.txt", integers, appendMode);
            if (shortStatMode || fullStatMode) printStatistics("Integers", integers, fullStatMode);
        }

        if (!floats.isEmpty()) {
            writeToFile(outputDirectory, filePrefix + "floats.txt", floats, appendMode);
            if (shortStatMode || fullStatMode) printStatistics("Floats", floats, fullStatMode);
        }

        if (!strings.isEmpty()) {
            writeToFile(outputDirectory, filePrefix + "strings.txt", strings, appendMode);
            if (shortStatMode || fullStatMode) printStatistics("Strings", strings, fullStatMode);
        }
    }

    /**
     * Вспомогательный метод задания основных опций для работы программы.
     *
     * @return экземпляр класса Options, содержащий все настройки опций.
     */
    private static Options getOptions() {
        Options options = new Options();

        Option inputOption = new Option("i", "input", true, "Input files (comma-separated)");
        inputOption.setRequired(true);
        options.addOption(inputOption);

        Option outputOption = new Option("o", "output", true, "Output directory");
        outputOption.setRequired(false);
        options.addOption(outputOption);

        Option prefixOption = new Option("p", "prefix", true, "Output file name prefix");
        prefixOption.setRequired(false);
        options.addOption(prefixOption);

        Option appendOption = new Option("a", "append", false, "Append to existing files");
        appendOption.setRequired(false);
        options.addOption(appendOption);

        Option shortStatOption = new Option("s", "short-stat", false, "Short statistics");
        shortStatOption.setRequired(false);
        options.addOption(shortStatOption);

        Option fullStatOption = new Option("f", "full-stat", false, "Full statistics");
        fullStatOption.setRequired(false);
        options.addOption(fullStatOption);

        return options;
    }

    /**
     * Метод записи собранных данных в файл.
     *
     * @param outputDirectory директория, куда будет записан файл с результатами.
     * @param fileName имя файла, в который будут записаны данные.
     * @param data список данных.
     * @param appendMode режим добавления (true) или перезаписи (false).
     */
    private static <T extends Comparable<T>> void writeToFile(String outputDirectory, String fileName, List<T> data, boolean appendMode) {
        File outputFile = new File(outputDirectory, fileName);
        try (PrintWriter writer = new PrintWriter(new FileWriter(outputFile, appendMode))) {
            for (T value : data) {
                writer.println(value.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для вывода статистики по данным на экран.
     *
     * @param dataType тип данных, по которому производится вывод статистики.
     * @param data список данных.
     * @param fullStatMode флаг, который показывает, нужно ли выводить полную статистику (true) или краткую (false).
     */
    private static <T extends Comparable<T>> void printStatistics(String dataType, List<T> data, boolean fullStatMode) {
        System.out.println("Statistic for " + dataType + ":");
        System.out.println("Number of elements: " + data.size());

        if (fullStatMode) {
            if (!data.isEmpty()) {
                if (data.get(0) instanceof Number) {
                    T min = Collections.min(data);
                    T max = Collections.max(data);

                    System.out.println("Min: " + min);
                    System.out.println("Max: " + max);

                    double sum = 0;
                    for (T value : data) {
                        sum += ((Number) value).doubleValue();
                    }
                    double average = sum / data.size();
                    System.out.println("Sum: " + sum);
                    System.out.println("Average: " + average);
                } else if (data.get(0) instanceof String) {
                    int minLength = ((String) data.get(0)).length();
                    int maxLength = ((String) data.get(0)).length();

                    for (T value : data) {
                        int length = ((String) value).length();
                        minLength = Math.min(minLength, length);
                        maxLength = Math.max(maxLength, length);
                    }

                    System.out.println("Min Length: " + minLength);
                    System.out.println("Max Length: " + maxLength);
                }
            }
        }
    }
}
