import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class DataSeparatorTest {
    @Test
    public void processFiles_ALL_TYPES_ONE_FILE() {
        // Создаем временную директорию.
        Path tempDir;
        try {
            tempDir = Files.createTempDirectory("DataSeparatorTest");
        } catch (IOException e) {
            throw new RuntimeException("Failed to create temporary directory", e);
        }

        // Создаем временные входные файлы.
        Path inputFile1 = createTempFileWithContent("1\n2.0\nabc\n3\n4.0\ndef", tempDir, "input1.txt");

        // Запускаем тестируемый метод.
        DataSeparator.processFiles(inputFile1.toString(), tempDir.toString(),
                "result_", false, false, false);

        // Проверяем результаты.
        assertFileContentEquals(Arrays.asList("1", "3"), tempDir.resolve("result_integers.txt"));
        assertFileContentEquals(Arrays.asList("2.0", "4.0"), tempDir.resolve("result_floats.txt"));
        assertFileContentEquals(Arrays.asList("abc", "def"), tempDir.resolve("result_strings.txt"));

        // Очищаем временные файлы.
        deleteTempFile(inputFile1);
        deleteTempFile(tempDir.resolve("result_integers.txt"));
        deleteTempFile(tempDir.resolve("result_floats.txt"));
        deleteTempFile(tempDir.resolve("result_strings.txt"));
        deleteTempFile(tempDir);
    }

    @Test
    public void processFiles_ALL_TYPES_THREE_FILE() {
        // Создаем временную директорию.
        Path tempDir;
        try {
            tempDir = Files.createTempDirectory("DataSeparatorTest");
        } catch (IOException e) {
            throw new RuntimeException("Failed to create temporary directory", e);
        }

        // Создаем временные входные файлы.
        Path inputFile1 = createTempFileWithContent("1\n2.2\nabc", tempDir, "input1.txt");
        Path inputFile2 = createTempFileWithContent("3\n4.4\ndef", tempDir, "input2.txt");
        Path inputFile3 = createTempFileWithContent("ghi\n6.6\n7", tempDir, "input3.txt");

        // Запускаем тестируемый метод.
        DataSeparator.processFiles(
                inputFile1.toString() + "," + inputFile2.toString() + "," + inputFile3.toString(), tempDir.toString(),
                "result_", false, false, false
        );

        // Проверяем результаты.
        assertFileContentEquals(Arrays.asList("1", "3", "7"), tempDir.resolve("result_integers.txt"));
        assertFileContentEquals(Arrays.asList("2.2", "4.4", "6.6"), tempDir.resolve("result_floats.txt"));
        assertFileContentEquals(Arrays.asList("abc", "def", "ghi"), tempDir.resolve("result_strings.txt"));

        // Очищаем временные файлы.
        deleteTempFile(inputFile1);
        deleteTempFile(inputFile2);
        deleteTempFile(inputFile3);
        deleteTempFile(tempDir.resolve("result_integers.txt"));
        deleteTempFile(tempDir.resolve("result_floats.txt"));
        deleteTempFile(tempDir.resolve("result_strings.txt"));
        deleteTempFile(tempDir);
    }

    @Test
    public void processFiles_EMPTY_FILE() {
        // Создаем временную директорию.
        Path tempDir;
        try {
            tempDir = Files.createTempDirectory("DataSeparatorTest");
        } catch (IOException e) {
            throw new RuntimeException("Failed to create temporary directory", e);
        }

        // Создаем временные входные файлы.
        Path inputFile1 = createTempFileWithContent("", tempDir, "input1.txt");

        // Запускаем тестируемый метод.
        DataSeparator.processFiles(inputFile1.toString(), tempDir.toString(),
                "result_", false, false, false);

        // Проверяем результаты.
        assertFalse(Files.exists(tempDir.resolve("result_integers.txt")));
        assertFalse(Files.exists(tempDir.resolve("result_floats.txt")));
        assertFalse(Files.exists(tempDir.resolve("result_strings.txt")));

        // Очищаем временные файлы.
        deleteTempFile(inputFile1);
        deleteTempFile(tempDir.resolve("result_integers.txt"));
        deleteTempFile(tempDir.resolve("result_floats.txt"));
        deleteTempFile(tempDir.resolve("result_strings.txt"));
        deleteTempFile(tempDir);
    }

    @Test
    public void processFiles_NO_INT_TYPE_ONE_FILE() {
        // Создаем временную директорию.
        Path tempDir;
        try {
            tempDir = Files.createTempDirectory("DataSeparatorTest");
        } catch (IOException e) {
            throw new RuntimeException("Failed to create temporary directory", e);
        }

        // Создаем временные входные файлы.
        Path inputFile1 = createTempFileWithContent("2.0\nabc\n4.0\ndef", tempDir, "input1.txt");

        // Запускаем тестируемый метод.
        DataSeparator.processFiles(inputFile1.toString(), tempDir.toString(),
                "result_", false, false, false);

        // Проверяем результаты.
        assertFalse(Files.exists(tempDir.resolve("result_integers.txt")));
        assertFileContentEquals(Arrays.asList("2.0", "4.0"), tempDir.resolve("result_floats.txt"));
        assertFileContentEquals(Arrays.asList("abc", "def"), tempDir.resolve("result_strings.txt"));

        // Очищаем временные файлы.
        deleteTempFile(inputFile1);
        deleteTempFile(tempDir.resolve("result_integers.txt"));
        deleteTempFile(tempDir.resolve("result_floats.txt"));
        deleteTempFile(tempDir.resolve("result_strings.txt"));
        deleteTempFile(tempDir);
    }

    @Test
    public void processFiles_NO_FLOAT_TYPE_ONE_FILE() {
        // Создаем временную директорию.
        Path tempDir;
        try {
            tempDir = Files.createTempDirectory("DataSeparatorTest");
        } catch (IOException e) {
            throw new RuntimeException("Failed to create temporary directory", e);
        }

        // Создаем временные входные файлы.
        Path inputFile1 = createTempFileWithContent("2\nabc\n4\ndef", tempDir, "input1.txt");

        // Запускаем тестируемый метод.
        DataSeparator.processFiles(inputFile1.toString(), tempDir.toString(),
                "result_", false, false, false);

        // Проверяем результаты.
        assertFalse(Files.exists(tempDir.resolve("result_floats.txt")));
        assertFileContentEquals(Arrays.asList("2", "4"), tempDir.resolve("result_integers.txt"));
        assertFileContentEquals(Arrays.asList("abc", "def"), tempDir.resolve("result_strings.txt"));

        // Очищаем временные файлы.
        deleteTempFile(inputFile1);
        deleteTempFile(tempDir.resolve("result_integers.txt"));
        deleteTempFile(tempDir.resolve("result_floats.txt"));
        deleteTempFile(tempDir.resolve("result_strings.txt"));
        deleteTempFile(tempDir);
    }

    @Test
    public void processFiles_NO_STRING_TYPE_ONE_FILE() {
        // Создаем временную директорию.
        Path tempDir;
        try {
            tempDir = Files.createTempDirectory("DataSeparatorTest");
        } catch (IOException e) {
            throw new RuntimeException("Failed to create temporary directory", e);
        }

        // Создаем временные входные файлы.
        Path inputFile1 = createTempFileWithContent("2.0\n2\n4.0\n4", tempDir, "input1.txt");

        // Запускаем тестируемый метод.
        DataSeparator.processFiles(inputFile1.toString(), tempDir.toString(),
                "result_", false, false, false);

        // Проверяем результаты.
        assertFileContentEquals(Arrays.asList("2", "4"), tempDir.resolve("result_integers.txt"));
        assertFileContentEquals(Arrays.asList("2.0", "4.0"), tempDir.resolve("result_floats.txt"));
        assertFalse(Files.exists(tempDir.resolve("result_strings.txt")));

        // Очищаем временные файлы.
        deleteTempFile(inputFile1);
        deleteTempFile(tempDir.resolve("result_integers.txt"));
        deleteTempFile(tempDir.resolve("result_floats.txt"));
        deleteTempFile(tempDir.resolve("result_strings.txt"));
        deleteTempFile(tempDir);
    }

    @Test
    public void processFiles_SCIENTIFIC_NOTATION() {
        // Создаем временную директорию.
        Path tempDir;
        try {
            tempDir = Files.createTempDirectory("DataSeparatorTest");
        } catch (IOException e) {
            throw new RuntimeException("Failed to create temporary directory", e);
        }

        // Создаем временные входные файлы.
        Path inputFile1 = createTempFileWithContent("1.0\n1.5\n2.0E+3\n5e+99\n4e-15", tempDir, "input1.txt");

        // Запускаем тестируемый метод.
        DataSeparator.processFiles(inputFile1.toString(), tempDir.toString(),
                "result_", false, false, false);

        // Проверяем результаты.
        assertFileContentEquals(Arrays.asList("1.0", "1.5", "2000.0", "5.0E99", "4.0E-15"), tempDir.resolve("result_floats.txt"));
        assertFalse(Files.exists(tempDir.resolve("result_integers.txt")));
        assertFalse(Files.exists(tempDir.resolve("result_strings.txt")));

        // Очищаем временные файлы.
        deleteTempFile(inputFile1);
        deleteTempFile(tempDir.resolve("result_integers.txt"));
        deleteTempFile(tempDir.resolve("result_floats.txt"));
        deleteTempFile(tempDir.resolve("result_strings.txt"));
        deleteTempFile(tempDir);
    }

    @Test
    public void processFiles_APPEND_OPTIONS() {
        // Создаем временную директорию.
        Path tempDir;
        try {
            tempDir = Files.createTempDirectory("DataSeparatorTest");
        } catch (IOException e) {
            throw new RuntimeException("Failed to create temporary directory", e);
        }

        // Создаем временные входные файлы.
        // Файлы результатов с уже имеющимися записанными в них данными.
        createTempFileWithContent("1\n2\n3\n", tempDir, "result_integers.txt");
        createTempFileWithContent("1.1\n2.2\n3.3\n", tempDir, "result_floats.txt");

        Path inputFile1 = createTempFileWithContent("1\n2.0\n3\n4.5", tempDir, "input1.txt");
        Path inputFile2 = createTempFileWithContent("5\n6.0\n7\n8.5", tempDir, "input2.txt");

        // Запускаем тестируемый метод без опции -a с фалом inputFile1. Данные должны перезаписаться.
        DataSeparator.processFiles(inputFile1.toString(), tempDir.toString(),
                "result_", false, false, false);

        // Запускаем тестируемый метод с опцией -a с фалом inputFile2. Данные должны добавиться в конец фалов.
        DataSeparator.processFiles(inputFile2.toString(), tempDir.toString(),
                "result_", true, false, false);

        // Проверяем результаты.
        assertFileContentEquals(Arrays.asList("1", "3", "5", "7"), tempDir.resolve("result_integers.txt"));
        assertFileContentEquals(Arrays.asList("2.0", "4.5", "6.0", "8.5"), tempDir.resolve("result_floats.txt"));
        assertFalse(Files.exists(tempDir.resolve("result_strings.txt")));

        // Очищаем временные файлы.
        deleteTempFile(inputFile1);
        deleteTempFile(inputFile2);
        deleteTempFile(tempDir.resolve("result_integers.txt"));
        deleteTempFile(tempDir.resolve("result_floats.txt"));
        deleteTempFile(tempDir.resolve("result_strings.txt"));
        deleteTempFile(tempDir);
    }

    @Test
    public void processFiles_NEW_PREFIX() {
        // Создаем временную директорию.
        Path tempDir;
        try {
            tempDir = Files.createTempDirectory("DataSeparatorTest");
        } catch (IOException e) {
            throw new RuntimeException("Failed to create temporary directory", e);
        }

        // Создаем временные входные файлы.

        Path inputFile1 = createTempFileWithContent("1\n2\n3", tempDir, "input1.txt");
        Path inputFile2 = createTempFileWithContent("4\n5\n6", tempDir, "input2.txt");

        // Вызываем без префикса
        DataSeparator.processFiles(inputFile1.toString(), tempDir.toString(),
                "", false, false, false);

        // Используем префикс result_
        DataSeparator.processFiles(inputFile2.toString(), tempDir.toString(),
                "result_", true, false, false);

        // Проверяем результаты.
        assertTrue(Files.exists(tempDir.resolve("integers.txt")));
        assertTrue(Files.exists(tempDir.resolve("result_integers.txt")));

        // Очищаем временные файлы.
        deleteTempFile(inputFile1);
        deleteTempFile(inputFile2);
        deleteTempFile(tempDir.resolve("integers.txt"));
        deleteTempFile(tempDir.resolve("result_integers.txt"));
        deleteTempFile(tempDir.resolve("result_floats.txt"));
        deleteTempFile(tempDir.resolve("result_floats.txt"));
        deleteTempFile(tempDir.resolve("result_strings.txt"));
        deleteTempFile(tempDir);
    }

    private Path createTempFileWithContent(String content, Path directory, String fileName) {
        Path filePath = directory.resolve(fileName);
        try {
            Files.write(filePath, content.getBytes());
        } catch (IOException e) {
            throw new RuntimeException("Failed to create temporary file:" + filePath, e);
        }
        return filePath;
    }

    private void assertFileContentEquals(List<String> expectedLines, Path filePath) {
        try {
            List<String> actualLines = Files.readAllLines(filePath);
            assertEquals(expectedLines, actualLines);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read file: " + filePath, e);
        }
    }

    private void deleteTempFile(Path filePath) {
        try {
            Files.deleteIfExists(filePath);
        } catch (IOException e) {
            throw new RuntimeException("Failed to delete file: " + filePath, e);
        }
    }
}
